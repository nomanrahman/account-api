var express = require('express');
var router = express.Router();
var config = require('../config/config');
var mailchimpService = require('../services/mailchimp');

router.post('/status', function(req, res, next) {
  var email = req.body.email;
  if(!email || email.length <= 3){
    next(new Error("Bad email address: " + email));
  }
  var newsletter1 = mailchimpService.getIdForNewsletter('newsletter1');
  if(newsletter1 === null){
    next(new Error("Invalid email list specified."));
  }
  var newsletter2 = mailchimpService.getIdForNewsletter('newsletter2');
  if(newsletter2 === null){
    next(new Error("Invalid email list specified."));
  }
  mailchimpService
    .checkSubscription(email, newsletter1)
    .then(function(result1) {
      mailchimpService
        .checkSubscription(email, newsletter2)
        .then(function(result2) {
          var subscribed1, subscribed2;
          if(result1.response.statusCode === '404') subscribed1 = 'false';
          else {
            if (result1.data.status === 'subscribed') subscribed1 = 'true';
            else subscribed1 = 'false';
          }
          if(result2.response.statusCode === '404') subscribed2 = 'false';
          else {
            if (result2.data.status === 'subscribed') subscribed2 = 'true';
            else subscribed2 = 'false';
          }
          console.log(JSON.stringify([subscribed1, subscribed2]))
          res.send(JSON.stringify([subscribed1, subscribed2]))
        })
    })

})

/* POST a new subscriber to an email list. */
router.post('/', function (req, res, next) {
  var email = req.body.email;
  if(!email || email.length <= 3){
    next(new Error("Bad email address: " + email));
  }

  var firstName = req.body.firstName;
  var lastName = req.body.lastName;
  var newsletter = req.body.newsletter;
  if(!newsletter || newsletter.length < 1){
    next(new Error("Bad email newsletter: " + newsletter));
  }
  var listId = mailchimpService.getIdForNewsletter(newsletter);
  if(listId === null){
    next(new Error("Invalid email list specified."));
  }

  mailchimpService
    .subscribeToList(email, firstName, lastName, listId)
    .then(function (result) {

      if(result.response.statusCode === 200){
        res.status(200).json({'message': 'success'});
      } else {
        // node-rest-client doesn't like to parse more complicated mime-types, so we do it here
        try{
          result.data = JSON.parse(result.data.toString());
        } catch(error){
          // error parsing message from json, that means it should already be a JSON object
        }
      }
      if(result.response.statusCode === 400&&result.data.title=='Member Exists'){
        // res.status(200).json({'message': 'Email already subscribed to list.'});
        mailchimpService
          .resubscribeToList(email, firstName, lastName, listId)
          .then(function(result2) {
            // Do nothing
          }).catch(function(error) {
            next(error);
        })
      } else {
        res.status(result.response.statusCode).json({'message': result.data.title || 'Unknown Error'});
      }
    }).catch(function (error) {
      next(error);
    }
  );
});

router.patch('/', function(req, res, next) {
    var newsletter = req.body.newsletter;
    var email = req.body.email;
    if(!newsletter || newsletter.length < 1){
        next(new Error("Bad email newsletter: " + newsletter));
    }
    var listId = mailchimpService.getIdForNewsletter(newsletter);
    if(listId === null){
        next(new Error("Invalid email list specified."));
    }
    mailchimpService
      .unsubscribeToList(email, listId)
      .then(function (result) {
        if(result.response.statusCode === 200){
          res.status(200).json({'message': 'success'});
        } else {
          // node-rest-client doesn't like to parse more complicated mime-types, so we do it here
          try{
              result.data = JSON.parse(result.data.toString());
          } catch(error){
              // error parsing message from json, that means it should already be a JSON object
          }
        }
      }).catch(function (error) {
        next(error);
    });
})

module.exports = router;



