var decrypt = require('./decrypt');

// load default properties
var config = require("./config-default");

// retrieve environment from system properties, then load environment specific config
var environment = process.env.TARGET_ENV;

if (environment) {
  envConfig = require("./config-" + environment);
  Object.assign(config, envConfig);
}

// decyrpt any encrypted config properties
config = decrypt.decryptProperties(config);

module.exports = config;