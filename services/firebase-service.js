var fbAdmin = require("firebase-admin");
var config = require('../config/config');
var fs = require('fs');

// load in firebase key and add private_key from config where it is decrypted
var fbKey = require('../config/fb-key');

config.firebase.key.private_key = config.firebase.key.private_key.replace(/\\n/g, "\n");

fbAdmin.initializeApp({
    credential: fbAdmin.credential.cert(config.firebase.key),
    databaseURL: "https://" + config.firebase.project + ".firebaseio.com"
});

var db = fbAdmin.database();
var ref = db.ref("users");
var refGallery = db.ref("gallery");

// How to manually reset a password for a user
// fbAdmin.auth().updateUser(uid, {
//     password: "password
// })
//     .then(function(userRecord) {
//         // See the UserRecord reference doc for the contents of userRecord.
//         console.log("Successfully updated user", userRecord.toJSON());
//     })
//     .catch(function(error) {
//         console.log("Error updating user:", error);
//     });


function backupFirebaseDatabase() {
  refGallery.once("value", function (snapshot) {
    var galleryData = snapshot.val();
    var galleryDataString = JSON.stringify(galleryData);
    fs.writeFile('galleryDataBackup.json', galleryDataString, 'utf8', function (err) {
      if (err) {
        console.log("Error backing up gallery database.");
        console.log(err);
      }
      else {
        console.log("Successfully backed up gallery data.");
      }
    });
  });
}

module.exports = {
  backupFirebaseDatabase: backupFirebaseDatabase
};