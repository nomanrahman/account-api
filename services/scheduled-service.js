var schedule = require('node-schedule');
var firebaseService = require('./firebase-service');

// backup firebase gallery data
schedule.scheduleJob('0 50 23 1/1 * ? *', function(){
  console.log('Starting gallery backup job...');
  try {
    firebaseService.backupFirebaseDatabase();
  } catch(err){
    console.log("Error running gallery backup.");
  }
  console.log('Finished gallery backup job...');
});